
<a name="1.1.0"></a>
## [1.1.0](https://gitlab.com/AdrianDC/gitlab-issues-sync/compare/1.0.0...1.1.0) (2022-08-11)

### Bug Fixes

* main: resolve GitLab global users list timeout with project
* main: minor improvements on existing label errors handling
* main: update requirements and add graceful failure

### Cleanups

* main: refactor codestyle and resolve lint warnings
* gitlab-ci: refactor the changelog generation job and tools
* gitlab-ci: refactor all jobs and move requirements to files
* readme: minor presentation improvements and cleanups
* setup: raise supported versions to Python 3.9 and 3.10
* setup: refactor against 'pexpect-executor' sources
* vscode: refactor against 'pexpect-executor' sources
* changelog: configure groups titles detailed map for chglog


<a name="1.0.0"></a>
## 1.0.0 (2020-03-18)

### CHANGELOG

* regenerate release tag changes history


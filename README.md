# gitlab-issues-sync

Synchronize issues from a GitLab project to another.

Using GitLab's Python API, this tools allows migrating  
and syncing issues, milestones and labels automatically.

---

## Usage

| Command            |                                                     |
| ------------------ | --------------------------------------------------- |
| gitlab-issues-sync | Synchronize issues from a GitLab project to another |

| Internal arguments |                        |
| ------------------ | ---------------------- |
| -h, --help         | Show this help message |

| Configuration arguments |                                                   |
| ----------------------- | ------------------------------------------------- |
| -i INPUT_GITLAB         | Input GitLab URL (default to https://gitlab.com)  |
| -o OUTPUT_GITLAB        | Output GitLab URL (default to https://gitlab.com) |

| Positional arguments |                                                            |
| -------------------- | ---------------------------------------------------------- |
| input_project        | Input project ID number                                    |
| output_project       | Output project ID number                                   |
| input_token          | Input project token credential                             |
| output_token         | Output project token credential (defaults to output_token) |

## Supported systems

|     Systems     | Supported |
| :-------------: | :-------: |
|  Linux (shell)  |   **✓**   |
|  macOS (shell)  |   **?**   |
| Windows (shell) |   **?**   |

---

## Dependencies

- [python-gitlab](https://pypi.org/project/python-gitlab/): Interact with GitLab API
- [setuptools](https://pypi.org/project/setuptools/): Build and manage Python packages

---

## References

- [git-chglog](https://github.com/git-chglog/git-chglog): CHANGELOG generator
- [gitlab-release](https://pypi.org/project/gitlab-release/): Utility for publishing on GitLab
- [gitlabci-local](https://pypi.org/project/gitlabci-local/): Launch .gitlab-ci.yml jobs locally
- [PyPI](https://pypi.org/): The Python Package Index
- [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/): Documentation for python-gitlab
- [twine](https://pypi.org/project/twine/): Utility for publishing on PyPI
